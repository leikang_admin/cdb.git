import logging
import time
import os



class logs:
    def __init__(self, msg, log_path, level='info'):
        '''
        此脚本用于统计打印日志内容
        :param msg:
        msg表示日志内容
        :param logpath:
        LogPath表示日志存放目录
        :param level:
        Level表示日志等级
        '''
        self.msg = msg
        self.level = level
        self.log_path = log_path
        if not os.path.isdir(self.log_path):
            os.mkdir(self.log_path)

    def ScriptLogs(self):
        """
        日志格式：
        :asctime 时间
        :pathname 日志文件路径
        :levelname 日志等级
        :message 日志内容
        :return:
        """
        logtime = time.strftime("%Y-%m-%d")
        logfile = f'{self.log_path}/{logtime}.log'
        logging.basicConfig(
            level=logging.INFO,
            filename=f'{logfile}',
            filemode='a',
            format='%(asctime)s - %(pathname)s[line:%(lineno)d] - %(levelname)s - %(message)s'

        )
        if self.level == 'info':
            logging.info(self.msg)
        elif self.level == 'error':
            logging.error(self.msg)
        else:
            logging.debug(self.msg)