import os
import script_logs
import json
from tencentcloud.common import credential
from tencentcloud.common.profile.client_profile import ClientProfile
from tencentcloud.common.profile.http_profile import HttpProfile
from tencentcloud.common.exception.tencent_cloud_sdk_exception import TencentCloudSDKException
from tencentcloud.cdb.v20170320 import cdb_client, models

root_path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'cdb')
print(root_path)
log_path = os.path.join(root_path, 'log')

class tencentid:
    """
    用于腾讯云MySQL实例最新冷备数据下载链接
    SecretId表示腾讯云API的id
    SecretKey表示腾讯云API的key
    Region表示腾讯云区域，如广州
    ID表示腾讯云MySQL实例ID
    """
    def __init__(self, secret_id, secret_key, region, id):
        self.secret_id = secret_id
        self.secret_key = secret_key
        self.region = region
        self.id = id
    def GetId(self):
        try:
            cred = credential.Credential(self.secret_id, self.secret_key)
            httpProfile = HttpProfile()
            httpProfile.endpoint = "cdb.tencentcloudapi.com"

            clientProfile = ClientProfile()
            clientProfile.httpProfile = httpProfile
            client = cdb_client.CdbClient(cred, self.region, clientProfile)

            req = models.DescribeBackupsRequest()
            params = {
                "InstanceId": self.id
            }
            req.from_json_string(json.dumps(params))

            resp = client.DescribeBackups(req)
            resps = resp.to_json_string()
            resp_dict = json.loads(resps)
            msg = "获取下载链接成功"
            logs = script_logs.logs(msg=msg, log_path=log_path, level='info')
            logs.ScriptLogs()
            return resp_dict['Items'][0]['IntranetUrl']
        except TencentCloudSDKException as err:
            logs = script_logs.logs(msg=err, log_path=log_path, level='error')
            logs.ScriptLogs()