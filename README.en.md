#### 介绍
数据备份：将腾讯云MySQL最新备份下载到本地


#### 软件架构
1. 软件架构说明
2. log 存放脚本日志
3. config.ini 存放基础配置
4. cdb_backup.py 数据备份执行脚本
5. delete_logs.py  清理之前备份数据脚本
6. get_tencentcloud_cdb_id.py  获取腾讯云MySQL实例的最新冷备下载地址
7. script_logs.py  日志打印


#### 安装教程

1.  python版本：3.6.8以上
2.  pip-3 install --upgrade httpx tdqm pymysql -i http://pypi.douban.com/simple --trusted-host pypi.douban.com
3.  腾讯云API接口文档地址：https://cloud.tencent.com/document/api/236/15842
4.  pip-3 install --upgrade tencentcloud-sdk-python -i http://pypi.douban.com/simple --trusted-host pypi.douban.com

#### 使用说明

1.  配置文件说明

```
[tencent_api]

#腾讯云API
SecretId=
SecretKey=
#腾讯云实例区域
Region=
#存放备份数据目录
Dirname=
#备份数据存放时间
FileDate=
#开启多线程
thread_num=
#以下这个配置根据实例情况填写，如：
#脚本执行时所需要传入的参数用于获取文件名和或者实例ID
[dsh]  
#文件名
FileName=
#腾讯云MySQL实例id
ID=
```

2.  脚本启动方式

```
python3  datebackup.py   dsh
#注意dsh主要用于获取config.ini配置文件中的dsh下的文件名和实例id，根据实际情况填写,dsh名称也是可以自主命名的
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
